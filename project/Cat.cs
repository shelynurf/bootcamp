namespace CatLibrary;
public class Cat

{
	//variable
	public bool isSmoothFur; // camel case
	public string name;
	public bool isMale;
	public int age;
	public bool isOwned;
	public string color;
	
	// method
	public void MakeSound() // pascal case
	
	{
		Console.WriteLine("Make Sound");
	}
	
	public void Scratch()
	
	{
		Console.WriteLine("Scratch");
	}
	
	public void Sleep()
	
	{
		Console.WriteLine("Sleep");
	}
	
	public void Eat()
	
	{
		Console.WriteLine("Eat");
	}
}